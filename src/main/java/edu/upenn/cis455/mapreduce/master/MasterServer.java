package edu.upenn.cis455.mapreduce.master;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.bolt.MapBolt;
import edu.upenn.cis.stormlite.bolt.PrintBolt;
import edu.upenn.cis.stormlite.bolt.ReduceBolt;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis.stormlite.spout.FileSpout;
import edu.upenn.cis.stormlite.spout.FileSpoutImplementation;
import edu.upenn.cis.stormlite.tuple.Fields;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static spark.Spark.*;

public class MasterServer {

    private static final String FILE_SPOUT = "FILE_SPOUT";
    private static final String MAP_BOLT = "MAP_BOLT";
    private static final String REDUCE_BOLT = "REDUCE_BOLT";
    private static final String PRINT_BOLT = "PRINT_BOLT";

    static Logger log = LogManager.getLogger(MasterServer.class);

    public static Map< String, WorkerStatus > workerStatusMap = new HashMap<>();

    public static void registerStatusPage() {
        get("/status", (request, response) -> {

            checkWorkerActivity();

            response.type("text/html");

            String res = "";
            int i = 0;

            for (WorkerStatus ws : workerStatusMap.values()) {
                res += String.format("%d: port=%d, status=%s, job=%s, keysRead=%s, keysWritten=%d, results=[%s]", i++,
                        ws.port, ws.status, ws.job, ws.keysRead, ws.keysWritten, ws.results) + "<br>\n";
            }

            String path = "src/main/resources/status.html";
            String content =  Files.readString(Path.of(path));

            return String.format(content, res);
        });

    }

    public static void checkWorkerActivity() {

        for (Map.Entry<String, WorkerStatus> ws: workerStatusMap.entrySet()) {

            long timeDiff =  new Date().getTime() - ws.getValue().lastRequestTime.getTime();
            if (timeDiff > 30000) {
//                TODO: can I remove while traversing it?
                workerStatusMap.remove(ws.getKey());
            }
        }

    }

   public static HttpURLConnection sendJob(String dest, String reqType, String job, String parameters) throws IOException {
        URL url = new URL(dest + "/" + job);

        log.info("Sending request to " + url.toString());

        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod(reqType);

        if (reqType.equals("POST")) {
            conn.setRequestProperty("Content-Type", "application/json");

            OutputStream os = conn.getOutputStream();
            byte[] toSend = parameters.getBytes();
            os.write(toSend);
            os.flush();
        } else
            conn.getOutputStream();

        return conn;
    }

    public static void createMapReduceConfig(Config config, String jobName, String className,
                                             String mapThread, String reduceThread, String inputDir, String outputDir) {
        // Job name
        config.put("job", jobName);

        // Class with map function
        config.put("mapClass", className);
        // Class with reduce function
        config.put("reduceClass", className);

        // Numbers of executors (per node)
        config.put("spoutExecutors", "1");
        config.put("mapExecutors", String.valueOf(mapThread));
        config.put("reduceExecutors", String.valueOf(reduceThread));

        // input and output directory
        config.put("inputDir", inputDir);
        config.put("outputDir", outputDir);

    }

    public static void registerSumbitJob() {
        post("/submitjob",((request, response) -> {

            checkWorkerActivity();

            String jobName = request.queryParams("jobname");
            String className = request.queryParams("classname");
            String inputDirectory = request.queryParams("input");
            String outputDirectory = request.queryParams("output");
            String mapThread = request.queryParams("map");
            String reduceThread = request.queryParams("reduce");

            TopologyBuilder tb = new TopologyBuilder();
            FileSpout fileSpout = new FileSpoutImplementation();
            MapBolt mapBolt = new MapBolt();
            ReduceBolt reduceBolt = new ReduceBolt();
            PrintBolt printBolt = new PrintBolt();

//          3.3.1 creating topology
            //TODO: for parallelism, what number should I use here for spout/bolt?
            //TODO: should the thread count be parallelism?
            tb.setSpout(FILE_SPOUT, fileSpout, 1);
            tb.setBolt(MAP_BOLT, mapBolt, Integer.parseInt(mapThread)).fieldsGrouping(FILE_SPOUT, new Fields("key"));
            tb.setBolt(REDUCE_BOLT, reduceBolt, Integer.parseInt(reduceThread)).fieldsGrouping(MAP_BOLT, new Fields("key"));
            tb.setBolt(PRINT_BOLT, printBolt, 1).firstGrouping(REDUCE_BOLT);
            Topology topo = tb.createTopology();


//          3.3.2 populate config

            int counter = 0;
            for (Map.Entry<String, WorkerStatus> ws: workerStatusMap.entrySet()) {

                Config config = new Config();
                config.put("workerList", String.format("[%s]", String.join("," , workerStatusMap.keySet())));
                config.put("workerIndex", String.valueOf(counter++));
                createMapReduceConfig(config, jobName, className, mapThread,
                        reduceThread, inputDirectory, outputDirectory);

                WorkerJob job = new WorkerJob(topo, config);
                ObjectMapper mapper = new ObjectMapper();
                mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);

                HttpURLConnection conn = sendJob(ws.getKey(), "POST", "/definejob",
                        mapper.writerWithDefaultPrettyPrinter().writeValueAsString(job));

                if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    throw new RuntimeException("Job definition request failed");
                }
            }

            for (Map.Entry<String, WorkerStatus> ws: workerStatusMap.entrySet()) {
                sendJob(ws.getKey(), "POST", "/runjob", "");
            }



            return "";

        }));
    }

    public static void shutDown() {
        get("/shutdown", ((request, response) ->  {

            checkWorkerActivity();

            for (Map.Entry<String, WorkerStatus> ws : workerStatusMap.entrySet()) {
                sendJob(ws.getKey(), "GET", "/shutdown", "");
            }

            return "shutdown!";
        }));
    }

    /**
     * The mainline for launching a MapReduce Master.  This should
     * handle at least the status and workerstatus routes, and optionally
     * initialize a worker as well.
     *
     * @param args
     */
    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Usage: MasterServer [port number]");
            System.exit(1);
        }

        int myPort = Integer.valueOf(args[0]);
        port(myPort);

        System.out.println("Master node startup, on port " + myPort);

        // TODO: you may want to adapt parts of edu.upenn.cis.stormlite.mapreduce.TestMapReduce here

        registerStatusPage();

        // TODO: route handler for /workerstatus reports from the workers
        get("/workerstatus", ((request, response) -> {

            checkWorkerActivity();

            try {
                String ip = request.ip();
                int port = Integer.parseInt(request.queryParams("port"));
                String status = request.queryParams("status");
                String job = request.queryParams("job");
                int keysRead = Integer.parseInt(request.queryParams("keysRead"));
                int keysWritten = Integer.parseInt(request.queryParams("keysWritten"));
                String results = request.queryParams("results");

                Date lastRequest = new Date();

                WorkerStatus ws = new WorkerStatus(ip, port, status, job, keysRead, keysWritten, results);
                ws.lastRequestTime = lastRequest;

                String combo = ip + ":" + request.queryParams("port");

                workerStatusMap.put(combo, ws);

            } catch (Exception e) {
                log.error(e, e);
            }


            return "Worker reporting status...";
        }));

        registerSumbitJob();
        shutDown();

    }
}

