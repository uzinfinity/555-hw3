package edu.upenn.cis455.mapreduce.worker;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.DistributedCluster;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis455.mapreduce.RunJobRoute;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import spark.Spark;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static spark.Spark.port;

/**
 * Simple listener for worker creation
 *
 * @author zives
 *
 */
public class WorkerServer {
    static Logger log = LogManager.getLogger(WorkerServer.class);

    static DistributedCluster cluster = new DistributedCluster();

    public static List<TopologyContext> contexts = new ArrayList<>();

    static List<String> topologies = new ArrayList<>();

    public static Config workerConfig;

    public static Storage db;
    public static String storageDirectory;
    public static String masterAddress;
    public static Thread bgThread;

    public WorkerServer(int myPort) throws MalformedURLException {

        log.info("Creating server listener at socket " + myPort);

        port(myPort);
//      start updating worker status
        backgroundStatusUpdate();

        final ObjectMapper om = new ObjectMapper();
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);

//      register Define Job
        Spark.post("/definejob", (req, res) -> {

            WorkerJob workerJob;

            try {
                workerJob = om.readValue(req.body(), WorkerJob.class);

//              4.5.1 store the config for mapBolt and ReduceBolt usage
                workerConfig = workerJob.getConfig();

                try {
                    log.info("Processing job definition request" + workerJob.getConfig().get("job") +
                            " on machine " + workerJob.getConfig().get("workerIndex"));
                    contexts.add(cluster.submitTopology(workerJob.getConfig().get("job"), workerJob.getConfig(),
                            workerJob.getTopology()));

                    // Add a new topology
                    synchronized (topologies) {
                        topologies.add(workerJob.getConfig().get("job"));
                    }
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                db = new Storage(storageDirectory);

                return "Job launched";
            } catch (IOException e) {
                e.printStackTrace();

                // Internal server error
                res.status(500);
                return e.getMessage();
            }

        });

        Spark.post("/runjob", new RunJobRoute(cluster));

        Spark.post("/pushdata/:stream", (req, res) -> {
            try {
                String stream = req.params(":stream");
                log.debug("Worker received: " + req.body());
                Tuple tuple = om.readValue(req.body(), Tuple.class);

                log.debug("Worker received: " + tuple + " for " + stream);

                // Find the destination stream and route to it
                StreamRouter router = cluster.getStreamRouter(stream);

                if (contexts.isEmpty())
                    log.error("No topology context -- were we initialized??");

                TopologyContext ourContext = contexts.get(contexts.size() - 1);

                // Instrumentation for tracking progress
                if (!tuple.isEndOfStream()) {
                    ourContext.incSendOutputs(router.getKey(tuple.getValues()));
                    router.executeLocally(tuple, ourContext, req.queryParams("executorId"));
                }else {
                    // TODO: handle tuple vs end of stream for our *local nodes only*
                    // Please look at StreamRouter and its methods (execute, executeEndOfStream, executeLocally, executeEndOfStreamLocally)
                    router.executeEndOfStreamLocally(ourContext, req.queryParams("executorId"));
                }

                return "OK";

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

                res.status(500);
                return e.getMessage();
            }

        });

        Spark.get("/shutdown",((request, response) -> {

            shutdown();

            return "";
        }));

    }

    public void backgroundStatusUpdate() {
//        TODO: send workerStatus back to masterserver every 10 seconds

        bgThread = new Thread(new BGStatusUpdate());
        bgThread.start();
    }

    public static void createWorker(Map<String, String> config) {
        if (!config.containsKey("workerList"))
            throw new RuntimeException("Worker spout doesn't have list of worker IP addresses/ports");

        if (!config.containsKey("workerIndex"))
            throw new RuntimeException("Worker spout doesn't know its worker ID");
        else {
            String[] addresses = WorkerHelper.getWorkers(config);
            String myAddress = addresses[Integer.valueOf(config.get("workerIndex"))];
            String masterAddress = addresses[Integer.valueOf(config.get("masterIndex"))];

            WorkerServer.masterAddress = masterAddress;

            log.debug("Initializing worker " + myAddress);

            URL url;
            try {
                url = new URL(myAddress);

//              TODO: how can I set up workerserver's master address and storage directory?
                new WorkerServer(url.getPort());
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public static void shutdown() {
        synchronized(topologies) {
            for (String topo: topologies)
                cluster.killTopology(topo);
        }

        cluster.shutdown();
    }

    /**
     * Simple launch for worker server.  Note that you may want to change / replace
     * most of this.
     *
     * @param args
     * @throws MalformedURLException
     */
    public static void main(String args[]) throws MalformedURLException {
        if (args.length < 3) {
            System.out.println("Usage: WorkerServer [port number] [master host/IP]:[master port]");
            System.exit(1);
        }

        int myPort = Integer.parseInt(args[0]);
        String masterAddress = args[1];
        String storageDirectory = args[2];


        System.out.println("Worker node startup, on port " + myPort + " (need to include master port, storage directory)");

        WorkerServer worker = new WorkerServer(myPort);
        worker.storageDirectory = storageDirectory;
        worker.masterAddress = masterAddress;

        // TODO: you may want to adapt parts of edu.upenn.cis.stormlite.mapreduce.TestMapReduce here
        // What should I adopt here?
    }
}
